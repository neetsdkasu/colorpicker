VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ColoePicker"
   ClientHeight    =   4470
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   6975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   298
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   465
   StartUpPosition =   2  '画面の中央
   Begin VB.PictureBox Picture5 
      Height          =   375
      Left            =   120
      ScaleHeight     =   21
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   21
      TabIndex        =   9
      Top             =   120
      Width           =   375
   End
   Begin VB.PictureBox Picture3 
      Height          =   375
      Left            =   600
      ScaleHeight     =   21
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   21
      TabIndex        =   7
      Top             =   120
      Width           =   375
   End
   Begin VB.HScrollBar HScroll1 
      Height          =   255
      LargeChange     =   20
      Left            =   120
      Max             =   0
      TabIndex        =   5
      Top             =   4080
      Width           =   6495
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   3495
      LargeChange     =   20
      Left            =   6600
      Max             =   0
      TabIndex        =   4
      Top             =   600
      Width           =   255
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Paste from Clipboard"
      Height          =   375
      Left            =   4680
      TabIndex        =   3
      Top             =   120
      Width           =   2175
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   2
      Text            =   "0"
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   1
      Text            =   "0"
      Top             =   120
      Width           =   1215
   End
   Begin VB.PictureBox Picture1 
      Height          =   3495
      Left            =   120
      ScaleHeight     =   229
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   429
      TabIndex        =   0
      Top             =   600
      Width           =   6495
      Begin VB.PictureBox Picture4 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H8000000C&
         BorderStyle     =   0  'なし
         Enabled         =   0   'False
         Height          =   975
         Left            =   2040
         ScaleHeight     =   65
         ScaleMode       =   3  'ﾋﾟｸｾﾙ
         ScaleWidth      =   105
         TabIndex        =   8
         Top             =   960
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.PictureBox Picture2 
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BorderStyle     =   0  'なし
         Height          =   975
         Left            =   0
         ScaleHeight     =   65
         ScaleMode       =   3  'ﾋﾟｸｾﾙ
         ScaleWidth      =   105
         TabIndex        =   6
         Top             =   0
         Visible         =   0   'False
         Width           =   1575
      End
   End
   Begin VB.Label Label1 
      Caption         =   "RGB"
      Height          =   375
      Left            =   3720
      TabIndex        =   10
      Top             =   120
      Width           =   495
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim LastClickX As Single
Dim LastClickY As Single
Dim RGBOrder As Boolean

Private Sub SwitchZoom(zoom As Boolean)
    If Picture2.Visible <> zoom Then
        Exit Sub
    End If
    Picture2.Enabled = Not zoom
    Picture2.Visible = Not zoom
    Picture4.Enabled = zoom
    Picture4.Visible = zoom
    VScroll1.Enabled = Not zoom
    HScroll1.Enabled = Not zoom
End Sub

Private Function SwapRGB(ByVal col As Long) As Long
    Dim r As Long, g As Long, b As Long
    r = col Mod 256
    col = col \ 256
    g = col Mod 256
    b = col \ 256
    SwapRGB = (r * 256 + g) * 256 + b
End Function

Private Sub Command1_Click()
    If Not Clipboard.GetFormat(ClipBoardConstants.vbCFBitmap) Then
        Exit Sub
    End If
    
    Call SwitchZoom(False)
    
    VScroll1.Value = 0
    HScroll1.Value = 0
    
    Set Picture2.Picture = Clipboard.GetData(ClipBoardConstants.vbCFBitmap)
    
    Dim h As Integer
    h = Picture2.Height - Picture1.ScaleHeight
    VScroll1.Max = IIf(h < 0, 0, h)
    
    Dim w As Integer
    w = Picture2.Width - Picture1.ScaleWidth
    HScroll1.Max = IIf(w < 0, 0, w)
    
End Sub

Private Sub Form_Load()
    Dim tmp As Long, dw As Single, dh As Single
    
    Do
        Picture1.Width = Picture1.Width + 1
        tmp = CLng(Picture1.ScaleWidth)
        dw = dw + 1
    Loop Until tmp Mod 4 = 0
    
    Do
        Picture1.Height = Picture1.Height + 1
        tmp = CLng(Picture1.ScaleHeight)
        dh = dh + 1
    Loop Until tmp Mod 4 = 0
    
    Picture4.Move 0, 0, Picture1.ScaleWidth, Picture1.ScaleHeight
    
    VScroll1.Left = Picture1.Left + Picture1.Width
    VScroll1.Height = Picture1.Height
    
    HScroll1.Top = Picture1.Top + Picture1.Height
    HScroll1.Width = Picture1.Width
    
    Command1.Left = VScroll1.Left + VScroll1.Width - Command1.Width
    
    Me.Width = Me.Width + dw * Screen.TwipsPerPixelX
    Me.Height = Me.Height + dh * Screen.TwipsPerPixelY
    
    RGBOrder = True
End Sub

Private Sub HScroll1_Change()
    Picture2.Left = -HScroll1.Value
End Sub

Private Sub HScroll1_Scroll()
    Call HScroll1_Change
End Sub

Private Sub Label1_DblClick()
    Dim col As Long
    col = SwapRGB(CLng(Text1.Text))
    RGBOrder = Not RGBOrder
    
    Label1.Caption = IIf(RGBOrder, "RGB", "BGR")
    
    Text1.Text = Format(col)
    Text2.Text = Hex(col)
    
End Sub

Private Sub Picture2_DblClick()
    Picture4.Cls
    Dim px As Single, py As Single, w As Single, h As Single
    w = Int(Picture4.Width / 4!)
    h = Int(Picture4.Height / 4!)
    px = LastClickX - Int(w / 2!)
    py = LastClickY - Int(h / 2!)
    Call Picture4.PaintPicture( _
        Picture2.Picture, 0, 0, Picture4.Width, Picture4.Height, _
        px, py, w, h)
    Call SwitchZoom(True)
End Sub

Private Sub Picture2_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim col As Long
    col = Picture2.Point(X, Y)
    Picture3.BackColor = col
    If RGBOrder Then
        col = SwapRGB(col)
    End If
    Text1.Text = Format(col)
    Text2.Text = Hex(col)
    LastClickX = X
    LastClickY = Y
End Sub

Private Sub Picture2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Picture5.BackColor = Picture2.Point(X, Y)
End Sub

Private Sub Picture4_DblClick()
    Call SwitchZoom(False)
End Sub

Private Sub Picture4_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim col As Long
    col = Picture4.Point(X, Y)
    Picture3.BackColor = col
    If RGBOrder Then
        col = SwapRGB(col)
    End If
    Text1.Text = Format(col)
    Text2.Text = Hex(col)
End Sub

Private Sub Picture4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Picture5.BackColor = Picture4.Point(X, Y)
End Sub

Private Sub VScroll1_Change()
    Picture2.Top = -VScroll1.Value
End Sub

Private Sub VScroll1_Scroll()
    Call VScroll1_Change
End Sub
